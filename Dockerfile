FROM golang:latest AS build-env

ENV GO111MODULE=on

WORKDIR /go/src/app

COPY . .

RUN go build -o /bin/plop_meter_exporter


FROM debian:latest

COPY --from=build-env /bin/plop_meter_exporter /bin/prometheus_plop_meter_exporter

EXPOSE 8080/tcp

ENTRYPOINT ["/bin/prometheus_plop_meter__exporter"]

