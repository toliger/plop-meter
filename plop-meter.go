package main

import (
	"flag"
	"log"
	"net/http"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)



var (
    addr	= flag.String("listen-address", ":8080", "The address to listen on for HTTP requests.")
)

var (
    countedPlops =  promauto.NewCounterVec(prometheus.CounterOpts{
                Name: "plopmeter_counted_plops_total",
                Help: "The total number of counted plops",
	    },
		[]string{"cuve"},
    )
)

func main() {
	flag.Parse()

	countedPlops.WithLabelValues("cuve1").Add(1)
	countedPlops.WithLabelValues("cuve2").Add(3)

	http.Handle("/metrics", promhttp.Handler())
	log.Fatal(http.ListenAndServe(*addr, nil))
}
